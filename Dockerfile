FROM docker.io/aarizpe10/alpine-maven:1.0.0 as build-image
COPY app /app/
WORKDIR app
RUN mvn clean package -DskipTests
FROM docker.io/aarizpe10/alpine-tomcat:1.0.0
COPY --from=build-image /app/target/docker-spring-boot.war /usr/local/tomcat/webapps/docker-spring-boot.war