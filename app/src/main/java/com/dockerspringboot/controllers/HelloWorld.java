package com.dockerspringboot.controllers;

import java.util.Collections;
import java.util.Map;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class HelloWorld {

    @GetMapping("hello")
    public Map<String, String>hello(){
        return Collections.singletonMap("response", "Hello World!!");
    }
    
}